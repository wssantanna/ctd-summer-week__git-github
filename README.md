#CTD Summer Week - Controle versão

## Lista de comandos Git

### Objetivo 
Acesso a uma lista dos principais comandos utilizados no Git localmente e remotamente.

### Lista de comandos

#### **A. Configurações**

#### `git config –-global user.name “Digite seu nome ou como deseja ser identificado”`
Para definir como deseja ser identificado em suas alterações (commits).

#### `git config –-global user.email “usuario@provedor.com”`
Para definir qual o e-mail padrão utilizado em sua identificação de alterações (commits). 

#### `git config –-global core.editor code`
Para definir qual será o editor padrão. Nesse exemplo utilizamos o VSCode.

#### `git config –-list`
Para retornar todos os valores de configuração do git.

#### **B. Fluxo básico de controle de versão (local)**

#### `git init`
Para iniciar o controle de versão de todos os arquivos que existem na pasta do projeto. **Observação:** É necessário iniciar o versionamento apenas uma vez.

#### `git status`
Para saber em qual estágio estão os arquivos que existem na pasta.

#### `git add NOME_DO_ARQUIVO.extensão NOME_DO_ARQUIVO.extensão`
Para selecionar os arquivos que fazem sentido para o comentário das alterações realizadas (commits).

#### `git commit –m “Descrição da alteração realizada”`
Para salvar e incluir uma descrição sobre as alterações realizadas.

#### `git log`
Para listar todos as alterações salvar no versionamento local.

#### **C. Fluxo básico de controle de versão (remota)**

#### `git pull`
Para listar todos as alterações salvar no versionamento local.

#### `git push`
Para enviar para o repositório remoto as alterações realizadas localmente.